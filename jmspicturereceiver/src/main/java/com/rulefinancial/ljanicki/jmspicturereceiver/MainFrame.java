package com.rulefinancial.ljanicki.jmspicturereceiver;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteOrder;

import javax.imageio.ImageIO;
import javax.imageio.stream.IIOByteBuffer;
import javax.imageio.stream.ImageInputStream;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.activemq.protobuf.compiler.JavaGenerator;
import org.apache.commons.io.IOUtils;

import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;

/**
 * @author <a href="mailto:Lukasz.Janicki@rulefinancial.com">Lukasz Janicki</a>
 */
public class MainFrame extends JFrame {

	private TextArea chatArea;

	private Communication communication;

	public MainFrame() {
		setResizable(false);
		setSize(new Dimension(50, 50));
		setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		communication = new Communication();
		communication.establishConnection();
		prepareWindow();
		// drawMockPicture();
	}

	/**
	 * 
	 */
	private void drawMockPicture() {
		try {
			byte[] byteArray = IOUtils.toByteArray(new FileReader(new File(
					"C:/rule_finacial-mini-140.jpg")));

			BufferedImage image = ImageIO.read(new ByteArrayInputStream(
					byteArray));
			drawPicture(image);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void drawPicture(BufferedImage image) {
		PicPanel panel;
		JFrame frame = new JFrame();
		panel = new PicPanel(image);
		frame.setContentPane(panel);
		frame.setSize(image.getWidth(), image.getHeight());
		frame.setVisible(true);
	}

	/**
	 * 
	 */
	private void prepareWindow() {
		add(new JLabel("waiting for a picture..."));

		communication.setMessageHandler(new MessageListener() {
			public void onMessage(Message message) {
				if (message instanceof BytesMessage) {
					BytesMessage byteMessage = (BytesMessage) message;
					try {

						System.out.println("1");

						ByteArrayOutputStream outBuf = new ByteArrayOutputStream();
						int i;
						while ((i = byteMessage.readInt()) != -1) {
							outBuf.write(i);
						}
						outBuf.close();

						drawPicture(ImageIO.read(new ByteArrayInputStream(
								outBuf.toByteArray())));
						JOptionPane.showMessageDialog(MainFrame.this,
								"Received a picture", "Received",
								JOptionPane.INFORMATION_MESSAGE);
						
					} catch (JMSException e) {
						JOptionPane.showMessageDialog(MainFrame.this,
								e.getMessage(), "Error",
								JOptionPane.ERROR_MESSAGE);
						System.out.println(e);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
	}
}
