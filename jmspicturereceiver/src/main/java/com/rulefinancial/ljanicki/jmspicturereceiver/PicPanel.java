package com.rulefinancial.ljanicki.jmspicturereceiver;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

/**
 * @author <a href="mailto:Lukasz.Janicki@rulefinancial.com">Lukasz Janicki</a>
 * 
 */
public class PicPanel extends JPanel {

	private BufferedImage image;

	public PicPanel(BufferedImage image) {
		this.image = image;
	}

	@Override
	public void paintComponent(Graphics g) {
		g.drawImage(image, 0, 0, null);
	}

}
